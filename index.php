<?php

require __DIR__ . '/autoload.php';

$route = \App\Router::locate();

try {
    if (false !== $route) {
        $controller = new $route['controller'];
        $controller->action($route['action']);
    } else {
        throw new \App\Exceptions\Http404Exception('Страница ' . $_SERVER['REQUEST_URI'] . ' не найдена');
    }

} catch (\App\Exceptions\Http404Exception $exception) { // Исключения при работе с 404 ошибкой (логирование в конструкторе \App\Exceptions\LogableExceptions)

    $controller = new \App\Controllers\Error();
    $controller->action('404', $exception);

} catch (\Exception $exception) {   // все остальное - ошибки БД (лог аналогично 404), мультиисключения, не пойманные и тд.

    $controller = new \App\Controllers\Error();
    $controller->action('Default', $exception);

}