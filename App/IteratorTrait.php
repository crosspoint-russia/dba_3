<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 17.01.2017
 * Time: 14:50
 */

namespace App;


trait IteratorTrait
{
    protected $data = [];

    public function current()
    {
        return current($this->data);
    }

    public function next()
    {
        next($this->data);
    }

    public function key()
    {
        return key($this->data);
    }

    public function valid()
    {
        return null !== key($this->data);
    }

    public function rewind()
    {
        reset($this->data);
    }

}