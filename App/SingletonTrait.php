<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 13.01.2017
 * Time: 11:11
 */

namespace App;


trait SingletonTrait
{
    protected static $instance = null;

    protected function __construct()
    {
    }
    protected function __clone()
    {
    }
    protected function __wakeup()
    {
    }

    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static;
        }
        return static::$instance;
    }

}