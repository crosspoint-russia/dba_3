<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 30.01.2017
 * Time: 12:51
 */

namespace App\Controllers;


use App\Controller;

class Error extends Controller
{
    public function actionDefault($error)
    {
        $this->view->error = $error;
        $this->view->display($this->templatePath . '/error.php');
    }
    
    public function action404()
    {
        $this->view->display($this->templatePath . '/404.php');
    }
}