<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 18.04.2017
 * Time: 10:44
 */

namespace App\Controllers;


use App\Controller;
use App\Models\Item;

class Load extends Controller
{
    public function actionDefault()
    {
        // инициализация библиотеки
        $faker = \Faker\Factory::create();

        // лимит заполнения
        if (!empty($_GET['items'])) {
            $fillLimit = (int) $_GET['items'];
        } else {
            $fillLimit = 1000;
        }


        // заполняем и сохраняем модель
        $saved = 0;
        while ($fillLimit > 0) {
            $item = new Item();

            // подмешиваем записи с тестом
            if ($faker->boolean()) {
                $item->sku = $faker->unique()->numberBetween(1000, 999999);
            } else {
                $item->sku = 'TEST' . $faker->unique()->numberBetween(1000, 999999);
            }

            $item->title = $faker->name;
            $item->price = $faker->randomFloat(2, 100, 15000);
            $item->old_price = $faker->randomFloat(2, 100, 500);
            $item->image = $faker->imageUrl();
            $item->created_at = $faker->date();
            $item->quantity = $faker->randomDigitNotNull;

            if ($item->save()) {
                $saved++;
            }

            unset($item);

            $fillLimit--;
        }


        $this->view->savedItems = $saved;
        $this->view->display($this->templatePath . '/finished.php');
    }
}