<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 30.01.2017
 * Time: 12:36
 */

namespace App;


class ErrorLogger
{
    public $path = null;
    public function __construct()
    {
        $config = Config::getInstance();
        $this->path = $config->data['logPath'];
    }
    public function log(\Exception $exception)
    {
        $error['date'] = date('d-m-Y H:i:s');
        $error['message'] = 'error: ' . $exception->getMessage();
        $error['file'] = 'in file: ' . $exception->getFile();
        $error['line'] = 'on line: ' . $exception->getLine();
        $error['trace'] = 'trace: ' . "\n" . $exception->getTraceAsString();

        $logMessage = implode("\n", $error);
        $logMessage .= "\n" . '----------------' . "\n";

        file_put_contents($this->path, $logMessage, FILE_APPEND);
    }
}