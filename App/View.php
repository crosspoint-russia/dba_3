<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 17.01.2017
 * Time: 14:40
 */

namespace App;


class View
    implements \Countable, \Iterator
{
    use MagicTrait;
    use CountableTrait;
    use IteratorTrait;


    public function display($template) {
        foreach ($this->data as $key => $value) {
            $$key = $value;
        }

        include $template;
    }

    public function render($template) {
        foreach ($this->data as $key => $value) {
            $$key = $value;
        }

        ob_start();
        include $template;
        $content = ob_get_contents();
        ob_end_clean();

        return $content;
    }

}