<?php

namespace App\Models;

use App\Model;

class Item
    extends Model
{

    public static $table = 'items';

    public static $fillable = [
        'sku',
        'title',
        'price',
        'old_price',
        'image',
        'created_at',
        'quantity',
    ];

}